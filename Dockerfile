FROM java:8-alpine

ADD target/meetup-0.0.1.jar app.jar

EXPOSE 8080

CMD java -jar app.jar