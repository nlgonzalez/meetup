package la.ingenia.meetup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@SpringBootApplication
@ComponentScan("la.ingenia.meetup.services")
@RestController
public class MeetupApplication {

	public static void main(String[] args) {
		SpringApplication.run(MeetupApplication.class, args);
	}

	@GetMapping("/health")
	@Produces(MediaType.APPLICATION_JSON)
	public Response health() {
		return Response.ok().build();
	}

}


