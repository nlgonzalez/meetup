package la.ingenia.meetup.services;

import org.springframework.web.bind.annotation.GetMapping;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by nlgonzalez on 28/11/2018.
 */


@Path("/health")
public interface HealthService {

    @GET
    @Path("/")
    @GetMapping("/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response checkHealth();


}
