package la.ingenia.meetup.services.impl;

import la.ingenia.meetup.services.HealthService;

import javax.ws.rs.core.Response;

/**
 * Created by nlgonzalez on 28/11/2018.
 */
public class HealthServiceImpl implements HealthService {
    @Override
    public Response checkHealth() {

        return Response.ok().build();
    }

}
